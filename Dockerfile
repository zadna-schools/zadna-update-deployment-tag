# syntax=docker/dockerfile:1

FROM golang:1.16-alpine as builder

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY *.go ./

RUN go build -o /zadna-update-deployment-tag

FROM alpine:3.16.2
COPY --from=builder /zadna-update-deployment-tag .

ENV CI_PROJECT_NAME=
ENV CI_COMMIT_SHORT_SHA=

CMD /zadna-update-deployment-tag ${CI_PROJECT_NAME} ${CI_COMMIT_SHORT_SHA}