# zadna-update-deployment-tag

This is a container image that runs after a CI/CD build has deployed a new school image.

It updates the image tag in the deployment YAML for the school.

The updated tag is detected by ArgoCD and the update is applied.