package main

import (
	"fmt"
	"log"
	"os"

	"github.com/xanzy/go-gitlab"
)

func getDeploymentProjectID(projectName string) int {
	git, err := gitlab.NewClient("glpat-767qL7ksJm8tx6i9ivSj")
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	projects, _, err := git.Projects.ListProjects(&gitlab.ListProjectsOptions{
		Owned:  gitlab.Bool(true),
		Search: gitlab.String(projectName + "-app")})
	if err != nil {
		log.Fatalf("Failed to list Projects: %v", err)
	}

	projectID := 0
	if len(projects) == 1 {
		projectID = projects[0].ID
	} else {
		log.Fatalf("Failed to find deployment project for %v", projectName)
	}
	return projectID
}

func updateDeployment(projectAppPID int, projectName string, newTag string) {
	fileContent :=
		"apiVersion: apps/v1\n" +
			"kind: Deployment\n" +
			"metadata:\n" +
			"  name: " + projectName + "\n" +
			"  namespace: zaschools\n" +
			"spec:\n" +
			"  selector:\n" +
			"    matchLabels:\n" +
			"      app: " + projectName + "\n" +
			"  replicas: 1\n" +
			"  template:\n" +
			"    metadata:\n" +
			"      labels:\n" +
			"        app: " + projectName + "\n" +
			"    spec:\n" +
			"      containers:\n" +
			"      - name: " + projectName + "\n" +
			"        image: registry.gitlab.com/zadna-schools/" + projectName + ":" + newTag + "\n" +
			"        resources:\n" +
			"          requests:\n" +
			"            memory: '64Mi'\n" +
			"            cpu: '250m'\n" +
			"          limits:\n" +
			"            memory: '128Mi'\n" +
			"            cpu: '500m'\n" +
			"        ports:\n" +
			"        - containerPort: 80"

	git, err := gitlab.NewClient("glpat-767qL7ksJm8tx6i9ivSj")
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	updateFileOptions := &gitlab.UpdateFileOptions{
		Branch:        gitlab.String("main"),
		Content:       gitlab.String(fileContent),
		CommitMessage: gitlab.String("Update image tag to " + newTag),
	}
	_, _, err = git.RepositoryFiles.UpdateFile(projectAppPID, "prod/deployment.yaml", updateFileOptions)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Updated deployment YAML with new tag", newTag, "in", projectName+"-app")
}

func main() {
	/*
	   This will update the image tag in the deployment YAML of the application repository of a project.
	   The project name is passed as the first argument and the image tag as the second.
	*/
	updateDeployment(getDeploymentProjectID(os.Args[1]), os.Args[1], os.Args[2])
}
